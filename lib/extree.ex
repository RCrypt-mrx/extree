defmodule Extree do
  def main(args) do
    {opts, commands, _err} = OptionParser.parse(args, strict: [help: :boolean, tabsize: :integer], aliases: [h: :help, t: :tabsize])
    if opts[:help] do
      usage()
    else
      path_to_lib = Path.join(Path.expand(commands), "lib")
      result = inspect_dir(path_to_lib, File.ls!(path_to_lib), 0, if(opts[:tabsize], do: opts[:tabsize], else: 2))
      |> Enum.sort_by(&(&1[:prio]), :desc)
      ready = display(result, length(result))
      splitter = "@#{:rand.uniform |> to_string |> String.trim("0.")}@"
      ["lib" | List.replace_at(ready, 0, String.trim(List.first(ready), "\n"))]
      |> Enum.join(splitter)
      |> String.split("#{splitter}\n")
      |> Enum.join(splitter)
      |> String.split(splitter)
      |> Enum.each(fn str -> IO.puts(str) end)
    end
  end

  defp inspect_dir(libpath, pending, nesting, tabsize) do
    Enum.map(pending, fn pend ->
      path = Path.join(libpath, pend)
      tab = String.duplicate(" ", tabsize)
      scanner = fn (path, vision) ->
        to_parse = File.read!(path)
        for ending <- [" do\n", ", do:"] do
          Enum.map(Regex.scan(Regex.compile!("#{tab}#{vision} .*#{ending}"), to_parse), fn [arr] ->
            String.trim(arr, ending) |> String.trim(tab)
          end)
        end |> Enum.concat
      end
      [
        name: String.split(path, "/") |> List.last,
        nesting: nesting
      ] ++ case [File.dir?(path), String.split(path, "/") |> List.last |> String.ends_with?("ex")] do
        [true, false] -> [
            type: :dir,
            prio: 1,
            nested: inspect_dir(path, File.ls!(path), nesting + 1, tabsize)
          ]
        [false, true] -> [
            type: :file,
            prio: 0,
            funcs: scanner.(path, "def") ++ scanner.(path, "defp")
          ]
        _ -> [
            type: :ukn_file,
            prio: 0
          ]
      end
    end)
  end

  defp display(resource, length_of_nested, nest \\ "│   ", is_last \\ false) do
    Enum.map(Enum.with_index(resource), fn {res, ix} ->
      nest_final = if is_last do
          "#{if res[:nesting] == 0, do: nest, else: String.duplicate(nest, res[:nesting] - 1)}    "
        else
          "#{String.duplicate(nest, res[:nesting])}"
        end
      "#{if ix == length(resource) - 1 && res[:nesting] == 0, do: "", else: "\n"}#{nest_final}#{
        if ix == length_of_nested - 1, do: "└──", else: "├──"
      } #{res[:name]}"<>
        case res[:type] do
          :dir ->
            "#{display(res[:nested], length(res[:nested]), nest, ix == length_of_nested - 1)}"
          :file ->
            "#{
              if length(res[:funcs]) == 0, do: "", else: Enum.map(Enum.with_index(res[:funcs]), fn {rs, i} ->
                "\n#{nest_final}#{
                  if(ix == length_of_nested - 1, do: "    ", else: nest)}#{
                    if i == length(res[:funcs]) - 1, do: "└──", else: "├──"} #{rs}"
              end)
            }"
          :ukn_file -> ""
        end
    end)
  end

  defp usage do
    IO.puts("""
    Usage: extree [relative/path/to/your/elixir/project/dir]

    Options:
    -h, --help      Write usage message
    -t, --tabsize   Your personal tab size used in the project, default is 2
    """)
  end
end
