# Extree

Recursive elixir project lib directory listing utility. Extree lists the files in the directory, and definitions in the files.

## Build
```
git clone https://gitlab.com/rcrypt-mrx/extree.git
cd extree
mix escript.build
ln -s ./extree /usr/sbin/extree
```

## Usage
```
extree [relative/path/to/your/elixir/project/dir]
```

## Options
```
-h, --help      Write usage message
-t, --tabsize   Your personal tab size used in the project, default is 2
```

## Output example
```
> extree .
lib
└── extree.ex
    ├── def main(args)
    ├── defp inspect_dir(libpath, pending, nesting, tabsize)
    └── defp display(resource, length_of_nested, nest \\ "│   ", is_last \\ false)
>
```
