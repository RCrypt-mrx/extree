defmodule Extree.MixProject do
  use Mix.Project

  def project do
    [
      app: :extree,
      version: "0.1.0",
      elixir: "~> 1.13",
      escript: [main_module: Extree],
      deps: deps()
    ]
  end

  def application do
    []
  end

  defp deps do
    []
  end
end
